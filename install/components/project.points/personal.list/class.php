<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Entity,
    Bitrix\Main\Application,
    Project\Points\Model\PointsTable;

class ProjectPointsPersonalList extends CBitrixComponent {

    public function executeComponent() {
        if (CUser::IsAuthorized() and Loader::includeModule('project.points')) {
            $this->arParams["COUNT"] = ceil($this->arParams["COUNT"] ?: 10);

            $arFilter = array(
                'USER_ID' => cUser::getId()
            );
            if ($this->arParams['WRAPPER']['IS_FULL']) {
                $this->arResult['TYPE']['']['NAME'] = 'Все';
                $param = array(
                    'select' => array(
                        'TYPE',
                    ),
                    'filter' => array(
                        'USER_ID' => cUser::GetID()
                    ),
                    'group' => array(
                        'TYPE',
                    ),
                );
                $rsData = PointsTable::getList($param);
                $rsData = new CDBResult($rsData);
                while ($arItem = $rsData->Fetch()) {
                    $name = $arItem['TYPE'];
                    switch ($arItem['TYPE']) {
                        case 'ARTICLES':
                            $name = 'Статьи';
                            break;
                        case 'BLOG':
                            $name = 'Комментарии';
                            break;
                        case 'NEWS':
                            $name = 'Новости';
                            break;
                        case 'ORDER':
                            $name = 'Покупки';
                            break;
                    }
                    $this->arResult['TYPE'][$arItem['TYPE']]['NAME'] = $name;
                }
                foreach ($this->arResult['TYPE'] as $key => $value) {
                    if ($this->arParams['WRAPPER']['FILTER'] == $key) {
                        $this->arResult['TYPE'][$key]['SELECTED'] = true;
                        if ($key) {
                            $arFilter['TYPE'] = $key;
                        }
                    }
                }
            }

            $this->arResult['POINTS'] = array();
            $param = array(
                'select' => array(
                    new Entity\ExpressionField('FOUND_ROWS', 'SQL_CALC_FOUND_ROWS %s', 'ID'),
                    'MESSAGE',
                    'POINTS',
                    'NAME',
                    'URL',
                ),
                'order' => array(
                    'ID' => 'DESC'
                ),
                'filter' => $arFilter,
                'limit' => $this->arParams["COUNT"],
                'offset' => ($this->arParams["WRAPPER"]['PAGEN'] - 1) * $this->arParams["COUNT"]
            );
            $rsData = PointsTable::getList($param);
            $rsData = new CDBResult($rsData);
            while ($arItem = $rsData->Fetch()) {
                $this->arResult['POINTS'][] = $arItem;
            }

            $count = Application::getConnection()->queryScalar('SELECT FOUND_ROWS() as TOTAL');
            $this->arResult['IS_NEXT'] = ($this->arParams["COUNT"] * $this->arParams["WRAPPER"]['PAGEN']) < $count;

            $this->includeComponentTemplate();
        }
    }

}
