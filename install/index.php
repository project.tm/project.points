<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Points\Model\PointsTable;

IncludeModuleLangFile(__FILE__);

class project_points extends CModule {

    public $MODULE_ID = 'project.points';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_POINTS_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_POINTS_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_POINTS_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallDB();
        $this->InstallEvent();
        $this->InstallFiles();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallDB();
        $this->UnInstallEvent();
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallDB() {
        Application::getInstance()->getConnection(PointsTable::getConnectionName())->query('CREATE TABLE IF NOT EXISTS ' . PointsTable::getTableName() . ' (
            ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            USER_ID INT,
            TYPE VARCHAR(255),
            IBLOCK_ID INT,
            ELEMENT_ID INT,
            POINTS INT,
            MESSAGE VARCHAR(255),
            NAME VARCHAR(255),
            URL VARCHAR(255)
        );');
    }

    public function UnInstallDB() {
//        Application::getInstance()->getConnection(PointsTable::getConnectionName())->query('DROP TABLE IF EXISTS ' . PointsTable::getTableName() . ';');
    }

    public function InstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, '\Project\Points\Event\Iblock', 'OnAfterIBlockElementUpdate');
        $eventManager->registerEventHandler('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID, '\Project\Points\Event\Iblock', 'OnAfterIBlockElementUpdate');
        $eventManager->registerEventHandler('iblock', 'OnBeforeIBlockElementDelete', $this->MODULE_ID, '\Project\Points\Event\Iblock', 'OnBeforeIBlockElementDelete');

        $eventManager->registerEventHandler('sale', 'OnSaleOrderPaid', $this->MODULE_ID, '\Project\Points\Event\Sale', 'OnSaleOrderPaid');

        $eventManager->registerEventHandler('blog', 'OnCommentAdd', $this->MODULE_ID, '\Project\Points\Event\blog', 'OnCommentAdd');
        $eventManager->registerEventHandler('blog', 'OnCommentDelete', $this->MODULE_ID, '\Project\Points\Event\blog', 'OnCommentDelete');
    }

    public function UnInstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, '\Project\Points\Event\Iblock', 'OnAfterIBlockElementUpdate');
        $eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID, '\Project\Points\Event\Iblock', 'OnAfterIBlockElementUpdate');
        $eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementDelete', $this->MODULE_ID, '\Project\Points\Event\Iblock', 'OnBeforeIBlockElementDelete');

        $eventManager->unRegisterEventHandler('sale', 'OnSaleOrderPaid', $this->MODULE_ID, '\Project\Points\Event\Sale', 'OnSaleOrderPaid');

        $eventManager->unRegisterEventHandler('blog', 'OnCommentAdd', $this->MODULE_ID, '\Project\Points\Event\blog', 'OnCommentAdd');
        $eventManager->unRegisterEventHandler('blog', 'OnCommentDelete', $this->MODULE_ID, '\Project\Points\Event\blog', 'OnCommentDelete');
    }

    /*
     * InstallFiles
     */

    public function InstallFiles($arParams = array()) {
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/project.points/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/project.points/' . $this->MODULE_ID . '/', true, true);
    }

    public function UnInstallFiles() {
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/project.points/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/project.points/' . $this->MODULE_ID . '/'); //css
    }

}
